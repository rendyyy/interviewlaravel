<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\ApiFormatter;
use App\Models\transaksiPenjualan;
use GrahamCampbell\ResultType\Success;
use Illuminate\Support\Facades\DB;


class penjualanController extends Controller
{
    public function index(Request $request)
    {
        $penjualan = transaksiPenjualan::all();
        $jumlahJual = transaksiPenjualan::count();
        $pendapatan = transaksiPenjualan::sum('harga');


        if(isset($request['tanggal1'])){
          $query = transaksiPenjualan::select('tglTransaksi', DB::raw("(SUM(harga)) as total"))->groupBy('tglTransaksi')->whereBetween('tglTransaksi',[$request['tanggal1'],$request['tanggal2']])->get();
        }else{
           $query = transaksiPenjualan::select('tglTransaksi', DB::raw("(SUM(harga)) as total"))->groupBy('tglTransaksi')->get(); 
        }


        $tglTransaksi = [];
        $total = [];
        foreach ($query as $value) {
            $tglTransaksi[] = $value['tglTransaksi'];
            $total[] = $value['total'];
   
        }

        $data['tanggal'] = json_encode($tglTransaksi);
        $data['total'] = json_encode($total);

        return view('pages.home', [
            'penjualan' => $penjualan,
            'jumlahJual' => $jumlahJual,
            'pendapatan' => $pendapatan,
            'data'         =>$data, 
        ]);
        
    }

    public function search(Request $request)
    {
    $output = "";
    $penjualan = transaksiPenjualan::where('namaBrg', 'Like', '%'.$request->search.'%')->orWhere('harga', 'Like', '%'.$request->search.'%')->orWhere('tglTransaksi', 'Like', '%'.$request->search.'%')->get();

    foreach($penjualan as $penjualan)
    {
        $output.=

        '<tr>

        <td> '.$penjualan->namaBrg.'</td>
        <td class="font-weight-bold"> Rp.'.number_format($penjualan->harga).'</td>
        <td> '.date('d-m-Y', strtotime($penjualan->tglTransaksi )).'</td>

        </tr>';
    }    
    

    return response($output);

    }

    public function terimaPenjualan()
    {
         $dataTransaksi = transaksiPenjualan::all();

         if($dataTransaksi){
            return ApiFormatter::createApi(200,'success', $dataTransaksi);
        }else{
            return ApiFormatter::createApi(400,'failed');
        }
    }

    public function kirimPenjualan(Request $request)
    {
        $dataTransaksi = new transaksiPenjualan;
 
        $dataTransaksi->namaBrg = $request->namaBrg;
        $dataTransaksi->harga = $request->harga;
        $dataTransaksi->tglTransaksi = $request->tglTransaksi;
 
        $dataTransaksi->save();
        
        return 'Success';
    }

    public function updatePenjualan(Request $request, $id = null)
    {
        $dataTransaksi = transaksiPenjualan::find($id);
 
        $dataTransaksi->namaBrg = $request->namaBrg;
        $dataTransaksi->harga = $request->harga;
        $dataTransaksi->tglTransaksi = $request->tglTransaksi;
 
        $dataTransaksi->save();
        
        return 'Success';
    }
}
