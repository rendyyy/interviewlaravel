<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transaksiPenjualan extends Model
{
    use HasFactory;

    protected $fillable = [
        'namaBrg', 'harga', 'tglTransaksi'
    ];
}
