<?php

namespace App\Helpers;

class ApiFormatter{
    protected static $response = [
        'code' => null,
        'message' => null,
        'dataTransaksi' => null,
    ];
    public static function createApi($code = null, $message = null, $dataTransaksi = null){
        self::$response['code'] = $code;
        self::$response['message'] = $message;
        self::$response['dataTransaksi'] = $dataTransaksi;

        return response()->json(self::$response, self::$response['code']);

    }
}

?>