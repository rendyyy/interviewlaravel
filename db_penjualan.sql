-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 17, 2022 at 03:23 PM
-- Server version: 5.7.33
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_penjualan`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_09_17_064229_create_transaksi_penjualans_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_penjualans`
--

CREATE TABLE `transaksi_penjualans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `namaBrg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `tglTransaksi` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaksi_penjualans`
--

INSERT INTO `transaksi_penjualans` (`id`, `namaBrg`, `harga`, `tglTransaksi`, `created_at`, `updated_at`) VALUES
(1, 'Baju Zara', 150000, '2022-08-02', '2022-09-17 00:53:26', '2022-09-17 08:13:37'),
(2, 'Baju H&M', 150000, '2022-08-02', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(3, 'Celana Zara', 450000, '2022-08-03', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(4, 'Sepatu Adidas', 950000, '2022-08-04', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(5, 'Sepatu Vans', 850000, '2022-08-05', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(6, 'Topi Nike', 90000, '2022-08-06', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(7, 'Jaket Bomber', 500000, '2022-08-07', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(8, 'Baju H&M', 150000, '2022-08-08', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(9, 'Celana Zara', 450000, '2022-08-09', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(10, 'Baju Uniqlo', 120000, '2022-08-10', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(11, 'Sepatu Adidas', 950000, '2022-08-11', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(12, 'Jaket Bomber', 500000, '2022-08-12', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(13, 'Topi Nike', 90000, '2022-08-13', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(14, 'Sepatu Vans	', 850000, '2022-08-14', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(15, 'Celana Zara', 450000, '2022-08-15', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(16, 'Baju Uniqlo', 120000, '2022-08-16', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(17, 'Sepatu Adidas', 950000, '2022-08-17', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(18, 'Sepatu Vans', 850000, '2022-08-18', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(19, 'Topi Nike', 90000, '2022-08-19', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(20, 'Baju H&M', 150000, '2022-08-20', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(21, 'Celana Zara', 450000, '2022-08-21', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(22, 'Jaket Bomber', 500000, '2022-08-22', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(23, 'Topi Nike', 90000, '2022-08-23', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(24, 'Sepatu Vans', 850000, '2022-08-24', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(25, 'Celana Zara', 450000, '2022-08-25', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(26, 'Sepatu Adidas', 950000, '2022-08-26', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(27, 'Baju H&M', 150000, '2022-08-27', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(28, 'Topi Nike', 90000, '2022-08-28', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(29, 'Sepatu Adidas', 950000, '2022-08-29', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(30, 'Jaket Bomber', 500000, '2022-08-30', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(31, 'Sepatu Vans	', 850000, '2022-08-31', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(32, 'Sepatu Adidas', 950000, '2022-08-10', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(33, 'Baju H&M', 150000, '2022-08-02', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(34, 'Celana Zara', 450000, '2022-08-07', '2022-09-17 00:53:26', '2022-09-17 00:53:26'),
(35, 'Sepatu Nike', 850000, '2022-08-20', '2022-09-17 08:01:09', '2022-09-17 08:01:09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `transaksi_penjualans`
--
ALTER TABLE `transaksi_penjualans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaksi_penjualans`
--
ALTER TABLE `transaksi_penjualans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
