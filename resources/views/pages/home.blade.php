@extends('layouts.app')

@section('title')
    Rendy Store - Admin
@endsection

@section('content')

<div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
       {{-- {{ navbar }} --}}
    @include('layouts.navbar')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">

        <div id="right-sidebar" class="settings-panel">
          <i class="settings-close ti-close"></i>
          <div class="tab-content" id="setting-content">
            <!-- To do section tab ends -->
          </div>
        </div>
        <!-- partial -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item">
              <a class="nav-link" href="index.html">
                <i class="icon-grid menu-icon"></i>
                <span class="menu-title">Dashboard</span>
              </a>
            </li>
          </ul>
        </nav>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-md-12 grid-margin">
                <div class="row">
                  <div class="col-12 col-xl-8 mb-4 mb-xl-0">
                    <h3 class="font-weight-bold">Welcome Admin</h3>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 grid-margin transparent">
                <div class="row">
                  <div class="col-md-6 mb-4 stretch-card transparent">
                    <div class="card card-tale">
                      <div class="card-body">
                        <p class="mb-4">Barang Terjual</p>
                        <p class="fs-30 mb-2">{{ $jumlahJual }} Barang</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 mb-4 stretch-card transparent">
                    <div class="card card-dark-blue">
                      <div class="card-body">
                        <p class="mb-4">Total Pendapatan</p>
                        <p class="fs-30 mb-2">Rp. {{ number_format($pendapatan) }}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <p class="card-title mb-5">Grafik Penjualan</p>
                      <div class="dropdown flex-md-grow-1 flex-xl-grow-0">
                       <form action="" method="post">
                        @csrf
                        <input type="date" name="tanggal1">
                        <input type="date" name="tanggal2">
                        <input type="submit" value="Filter" class="btn btn-primary">
                       </form>
                      </div>
                    </div>
                    <canvas id="myChart"></canvas>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h2 class=" mb-3 text-center">Transaksi Penjualan</h2>
                    <div class="col-4">
                        <input class="form-control mb-3" type="search" name="search" id="search" placeholder="Cari Data">
                      </div>
                    <div class="table-responsive">
                    
                    
                      <table class="table table-striped table-hover table-bordered" style="overflow: hidden">
                        <thead>
                          <tr class="text-center">
                            <th>Nama Barang</th>
                            <th>Harga</th>
                            <th>Tanggal Transaksi</th>
                          </tr>
                        </thead>
                        <tbody class="allData">
                            @foreach ($penjualan as $jual)
                            <tr >
                                <td>{{ $jual->namaBrg }}</td>
                                <td class="font-weight-bold">Rp. {{number_format( $jual->harga) }}</td>
                                <td> {{ date('d-m-Y', strtotime($jual->tglTransaksi ))}} </td>
                            </tr>
                            @endforeach
                        </tbody>
                         <tbody id="content" class="searchData"></tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
        
        {{-- @dd('$tglTransaksi') --}}
          

        {{-- footer --}}
        @include('layouts.footer')

          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
@endsection




