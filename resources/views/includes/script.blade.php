<!-- plugins:js -->
    <script src="vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="vendors/chart.js/Chart.min.js"></script>
    <script src="vendors/datatables.net/jquery.dataTables.js"></script>
    <script src="vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
    <script src="js/dataTables.select.min.js"></script>

    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="js/off-canvas.js"></script>
    <script src="js/hoverable-collapse.js"></script>
    <script src="js/template.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/todolist.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="js/dashboard.js"></script>
    <script src="js/Chart.roundedBarCharts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js" ></script>
    <!-- End custom js for this page-->

    <script>
        $('#search').on('keyup', function()
            {
                $value = $(this).val();

                if($value)
                {
                    $('.allData').hide();
                    $('.searchData').show();
                }else{
                    $('.allData').show();
                    $('.searchData').hide();
                }

                $.ajax({
                    type:'get',
                    url:'{{ URL::to('search') }}',
                    data:{'search':$value},

                    success:function(data)
                    {
                        console.log(data);
                        $('#content').html(data);
                    }
                });
            })
    </script>

    <script>
const ctx = document.getElementById('myChart').getContext('2d');
var tanggal = <?=$data['tanggal']?>;
var total = <?=$data['total']?>;
const myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: tanggal,
        datasets: [{
            label: 'Data Penjualan',
            data: total,
            backgroundColor: 
                'rgba(255, 99, 132, 0.2)',
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});
</script>