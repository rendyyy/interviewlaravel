<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />

    <title>@yield('title')</title>

    {{-- style link --}}
    @stack('prepend-style')
    @include('includes.style')
    @stack('addon-style')

  </head>
  <body>

    {{-- page content --}}
    @yield('content')

    {{-- script link --}}
    @stack('prepend-script')
    @include('includes.script')
    @stack('addon-script')

</body>
</html>

