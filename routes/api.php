<?php

use App\Http\Controllers\penjualanController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('dataTransaksi', [penjualanController::class, 'terimaPenjualan']);
Route::post('dataTransaksi', [penjualanController::class, 'kirimPenjualan']);
Route::put('dataTransaksi/{id?}', [penjualanController::class, 'updatePenjualan']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
