<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\transaksiPenjualan;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        transaksiPenjualan::create(
            [
            'namaBrg' => 'Baju Uniqlo',
            'harga' => '120000',
            'tglTransaksi' => '2022-08-01'
            ],
        );

        transaksiPenjualan::create([
            'namaBrg' => 'Baju H&M',
            'harga' => '150000',
            'tglTransaksi' => '2022-08-02'
        ]);

        transaksiPenjualan::create([
            'namaBrg' => 'Celana Zara',
            'harga' => '450000',
            'tglTransaksi' => '2022-08-03'
        ]);
        
        transaksiPenjualan::create(
            [
            'namaBrg' => 'Sepatu Adidas',
            'harga' => '950000',
            'tglTransaksi' => '2022-08-04'
            ],
        );

        transaksiPenjualan::create([
            'namaBrg' => 'Sepatu Vans',
            'harga' => '850000',
            'tglTransaksi' => '2022-08-05'
        ]);

        transaksiPenjualan::create([
            'namaBrg' => 'Topi Nike',
            'harga' => '90000',
            'tglTransaksi' => '2022-08-06'
        ]);
        
        transaksiPenjualan::create(
            [
            'namaBrg' => 'Jaket Bomber',
            'harga' => '500000',
            'tglTransaksi' => '2022-08-07'
            ],
        );

        transaksiPenjualan::create([
            'namaBrg' => 'Baju H&M',
            'harga' => '150000',
            'tglTransaksi' => '2022-08-08'
        ]);

        transaksiPenjualan::create([
            'namaBrg' => 'Celana Zara',
            'harga' => '450000',
            'tglTransaksi' => '2022-08-09'
        ]);

        transaksiPenjualan::create(
            [
            'namaBrg' => 'Baju Uniqlo',
            'harga' => '120000',
            'tglTransaksi' => '2022-08-10'
            ],
        );

        transaksiPenjualan::create([
            'namaBrg' => 'Sepatu Adidas',
            'harga' => '950000',
            'tglTransaksi' => '2022-08-11'
        ]);

        transaksiPenjualan::create([
            'namaBrg' => 'Jaket Bomber',
            'harga' => '500000',
            'tglTransaksi' => '2022-08-12'
        ]);

        transaksiPenjualan::create(
            [
            'namaBrg' => 'Topi Nike',
            'harga' => '90000',
            'tglTransaksi' => '2022-08-13'
            ],
        );

        transaksiPenjualan::create([
            'namaBrg' => 'Sepatu Vans	',
            'harga' => '850000',
            'tglTransaksi' => '2022-08-14'
        ]);

        transaksiPenjualan::create([
            'namaBrg' => 'Celana Zara',
            'harga' => '450000',
            'tglTransaksi' => '2022-08-15'
        ]);
        
        transaksiPenjualan::create(
            [
            'namaBrg' => 'Baju Uniqlo',
            'harga' => '120000',
            'tglTransaksi' => '2022-08-16'
            ],
        );

        transaksiPenjualan::create([
            'namaBrg' => 'Sepatu Adidas',
            'harga' => '950000',
            'tglTransaksi' => '2022-08-17'
        ]);

        transaksiPenjualan::create([
            'namaBrg' => 'Sepatu Vans',
            'harga' => '850000',
            'tglTransaksi' => '2022-08-18'
        ]);

        transaksiPenjualan::create(
            [
            'namaBrg' => 'Topi Nike',
            'harga' => '90000',
            'tglTransaksi' => '2022-08-19'
            ],
        );

        transaksiPenjualan::create([
            'namaBrg' => 'Baju H&M',
            'harga' => '150000',
            'tglTransaksi' => '2022-08-20'
        ]);

        transaksiPenjualan::create([
            'namaBrg' => 'Celana Zara',
            'harga' => '450000',
            'tglTransaksi' => '2022-08-21'
        ]);

        transaksiPenjualan::create(
            [
            'namaBrg' => 'Jaket Bomber',
            'harga' => '500000',
            'tglTransaksi' => '2022-08-22'
            ],
        );

        transaksiPenjualan::create([
            'namaBrg' => 'Topi Nike',
            'harga' => '90000',
            'tglTransaksi' => '2022-08-23'
        ]);

        transaksiPenjualan::create([
            'namaBrg' => 'Sepatu Vans',
            'harga' => '850000',
            'tglTransaksi' => '2022-08-24'
        ]);
        
        transaksiPenjualan::create([
            'namaBrg' => 'Celana Zara',
            'harga' => '450000',
            'tglTransaksi' => '2022-08-25'
        ]);
        
        transaksiPenjualan::create(
            [
            'namaBrg' => 'Sepatu Adidas',
            'harga' => '950000',
            'tglTransaksi' => '2022-08-26'
            ],
        );

        transaksiPenjualan::create([
            'namaBrg' => 'Baju H&M',
            'harga' => '150000',
            'tglTransaksi' => '2022-08-27'
        ]);

        transaksiPenjualan::create([
            'namaBrg' => 'Topi Nike',
            'harga' => '90000',
            'tglTransaksi' => '2022-08-28'
        ]);

        transaksiPenjualan::create(
            [
            'namaBrg' => 'Sepatu Adidas',
            'harga' => '950000',
            'tglTransaksi' => '2022-08-29'
            ],
        );

        transaksiPenjualan::create([
            'namaBrg' => 'Jaket Bomber',
            'harga' => '500000',
            'tglTransaksi' => '2022-08-30'
        ]);

        transaksiPenjualan::create([
            'namaBrg' => 'Sepatu Vans	',
            'harga' => '850000',
            'tglTransaksi' => '2022-08-31'
        ]);

        transaksiPenjualan::create(
            [
            'namaBrg' => 'Sepatu Adidas',
            'harga' => '950000',
            'tglTransaksi' => '2022-08-10'
            ],
        );

        transaksiPenjualan::create([
            'namaBrg' => 'Baju H&M',
            'harga' => '150000',
            'tglTransaksi' => '2022-08-02'
        ]);

        transaksiPenjualan::create([
            'namaBrg' => 'Celana Zara',
            'harga' => '450000',
            'tglTransaksi' => '2022-08-07'
        ]);
    }
}
